
export default {
  normal: {
    position: 'relative',
    color: '#fff',
    transition: '.5s',
    zIndex: '3',
  },
  hover: {
    color: '#fff',
    transform: 'rotateY(360deg)',
  },
  iconBackground: {
    width: '60px',
    height: '60px',
    textAlign: 'center',
    lineHeight: '60px',
    fontSize: '35px',
    margin: '0 10px',
    display: 'block',
    borderRadius: '50%',
    position: 'relative',
    overflow: 'hidden',
    border: '3px solid #fff',
    zIndex: '1',
    color: '#fff',
  },
  before: {
    backgroundColor: '#000000',
  },

};
