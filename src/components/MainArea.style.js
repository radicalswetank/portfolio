export default {
  mainbar: {
      display: 'flex',
      backgroundColor: '#111',
      color: 'white',
      minWidth:'70vw',
      alignItems:'center',
      fontSize:'14px',
      flexDirection: 'column',
      paddingTop:'25vh'

    },
  break: {
      display:'flex',
      flexBasis: '10%',
      width: '100%',
      height: '0'
    }
};  
