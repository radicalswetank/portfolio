export default {
  sidebar: {
    backgroundColor: '#202020',
    minWidth: '25vw',
    minHeight: '73.1vh',
    color: 'white',
    fontSize: '20px',
    paddingTop: '5rem',
    paddingLeft: '3rem',
    //        color: 'rgba(255,255,255,.5)',
  },
  head: {
    fontFamily: 'Montserrat, sans-serif',
    fontSize: '55px',
    fontWeight: 'bold',
  },
  subhead: {
    fontFamily: 'Arial',
    fontSize: '25px',
    fontWeight: 'lighter',
    //color: '#515151',
    opacity: '0.7',
    marginBottom: '1rem',
  },
  buttons: {
    lineHeight: '1.75rem',
  },
//  hoverButtons: { opacity: '1',color: 'red' }
};